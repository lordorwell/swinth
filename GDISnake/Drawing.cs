﻿/**
 * Drawing is a special class I created to hold the drawing functions
 * However because it's only called on graphics frames, I must pass
 * in the e.graphics from the original calling by ref
 *TODO:  FINISH THE RECTANGLES
 *      LENGTHEN THE TAIL
 *      STORE SETTINGS
 *      
  */
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Windows.Forms;

namespace Swinth
    
{
    //magnitude is a custom object that is only used statically to store the direction the next line needs to move
    class Magnitude
    {

        public static Point start = new Point(9,6);
        public static Point end = new Point(6,9);

    }
    class Line //custom object to keep attributes that I need in the line trail
    {
        public Line()
        {
            start = new Point(150, 50);  //x & y of one end of the line.
            end = new Point(50, 150);    //x & y of the other end of the line.
        }
        public Point start;
        public Point end;
        public int colorIndex; //line color index.  valid values = 0-2.  The draw routine looks the pen up
        public Color rgbColor = Color.Black;
    }

    class HudMessage //all the attributes necessary to display a single HUD line on screen
    {
        public HudMessage(String message)
        {
            hudMessage = message;
            hudCounter = getMaxCount();
        }
        public string hudMessage = "";
        public int hudCounter = 0;
        public int getMaxCount() { return 80; }

    }
    class Drawing
    {
        /*
         * default constructor.  It preloads some variables that can't be initialized with values.
         */
         static Drawing()
        {
            penThickness[0] = 4; //initialize the pens.  
            penThickness[1] = 4; //to save memory, they are only created once each.
            penThickness[2] = 4;

            trail = new Line[maxTrailLength];//max trail length = maxTrailLength
            for (int i = 0; i < maxTrailLength; i++)
                trail[i] = new Line();

            SetPen(4, 0); //set starting values for the three colors we cycle through.
            SetPen(11, 1);
            SetPen(5, 2);
        }

        private static int[] colorIndexes = new int[3]; //the three indexes used for the on-screen colors.
        private static int[] penThickness = new int[3]; //contains the color info for the three pens currently on screen
        private static int nextColorCycle = 50; //after 50 lines are drawn, it goes to the next color, then changes this value to a random # from 20 to 100
        private static int colorStepsTaken = 0;
        private static int drawMode = 0;

        private static Font font;

        private static bool paused = false;
        private static bool hudActive = false;
        private static bool hSplit = true;
        private static bool vSplit = true;
        private static bool reverse = false; //reverses lines on split on one side
        private static bool fadingTrail = false;
        private static bool psychedelicMode = false;
        private static bool gradientMode = false;
        private static bool showEndCaps = false;
        private static bool classicErase = true;

        public static int screenshotCounter = 1;
        private static int maxIncrement = 32;
        private static int minIncrement = 4;

        private static Random rnd = new Random();

        private static int width = 0; //for storing the dimensions of the draw area in
        private static int height = 0;

        private static HudMessage[] hudMessages = new HudMessage[0] ;


        private static Line[] trail;
        private static int trailLength = 72;
        private static int maxTrailLength =720;
        private static int trailStartMarker = 0; //which index are you looking at 
        private static int trailEndMarker = 0;
        private static int divisor = 1; // if you set this to one, they overlap, 2 they are separated.
        //a list of the only colors i'm interested in using (16 of them) in default mode
        private static List<Color> lstColors = new List<Color>(
            new Color []{Color.Gray, Color.Brown, Color.DarkRed, Color.DarkOrange, Color.Green, Color.Blue, Color.Yellow, Color.DarkCyan,
            Color.LightGray, Color.SaddleBrown, Color.Red, Color.Orange, Color.LightGreen, Color.LightBlue, Color.LightYellow, Color.Cyan});

        /// <summary>
        ///   
        /// DrawText does what it says:  It draws text on the screen.column defaults to -1 which means center it.
        /// I have hard-coded 40 columns into the design so you'll see some math based on that.
        /// There is an overload for it.  You can pass in the text color.
        ///
        /// </summary>
        /// <param name="text">The text you are drawing</param>
        /// <param name="e">Object representing the screen</param>
        /// <param name="row">Horizontal row on the screen to draw it on.</param>
        /// <param name="column">the starting position 0-40 (optional).  If omitted, the text is centered.</param>
        public static void DrawText(String text, ref PaintEventArgs e, int row, int column = -1)
        {
            DrawText(text, ref e, row, Color.FromArgb(128, 128, 255), column);
        }
        public static void DrawText(String text, ref PaintEventArgs e, int row, Color color, int column = -1)
        {   
            Brush brush = new SolidBrush(color);

            int startingPixel; //left side of letter location
       
            if (column == -1)
            {
                column = 21 - text.Length / 2;
            }
            for (int i = 0; i < text.Length; i++) //due to weirdness in scaling, i was required to draw a single character at a time to make it look right
            {
                startingPixel = (Program.onlyForm.BackgroundImage.Width  / 48 * (column + 3 + i));
                e.Graphics.DrawString(text[i].ToString(), font, brush, new Point(startingPixel, row * (int)(Program.onlyForm.BackgroundImage.Height / 25)));
            }
            brush.Dispose();
        }

        //setters
        public static void SetHudMessage(String message)
        {
            int ub = hudMessages.Length;
            Array.Resize(ref hudMessages, ub + 1);
            hudMessages[ub] = new HudMessage(message);
            
            hudActive = true;
        }
        
        public static void SetFont(Font font) { Drawing.font = font; }

        /// <summary>
        /// There are three pens.  This sets the color each of them uses and any particular time.
        /// </summary>
        /// <param name="colorNum">This is the index from the colorIndex array</param>
        /// <param name="index">This is which of the three pens to set the color for.</param>
        public static void SetPen(int colorNum, int index = 0)
        { //valid index = 0 to 2
            //valid colorNum = 0-15
            colorIndexes[index] = colorNum; //need it stored so i can cycle with it
        }
        //this toggles whether or not horizontal split happens.  Called from 'h' keypress
        public static void ToggleHSplit() { hSplit = !hSplit; SetHudMessage("Horizontal Split: " + hSplit.ToString()); }

        //this toggles whether or not vertical split happens.  Called from 'v' keypress
        public static void ToggleVSplit() { vSplit = !vSplit; SetHudMessage("Vertical Split: " + vSplit.ToString()); }

        public static void ToggleReverse() { reverse = !reverse; }

        //divisor defaults to 2.  This toggles it from 2 to 1.  Dividing by 1 has it draw full screen.
        public static void ToggleCollision()
        {
            divisor ^= 3;
            if (divisor == 1)
            {
                SetHudMessage("Collision OFF");
            }
            else
            {
                SetHudMessage("Collision ON");
            }
        }
        public static void TogglePaused() { Drawing.paused = !Drawing.paused; }
        public static void ToggleGradientMode()
        {
            gradientMode = !gradientMode;
            SetHudMessage("Gradient mode: " + gradientMode.ToString());
        }
        public static void ToggleFadingTrail()
        {
            if (Drawing.classicErase == true)
                ToggleClassicErase();
            fadingTrail = !fadingTrail;
            SetHudMessage("FadingTrail: " + fadingTrail.ToString());
        }
        public static void ToggleEndCaps()
        { showEndCaps = !showEndCaps; SetHudMessage("End Caps: " + showEndCaps.ToString()); }
        public static void ToggleClassicErase()
        {
            if (fadingTrail == true)
                ToggleFadingTrail();
            classicErase = !classicErase;
            SetHudMessage("Classic Erase = " + classicErase.ToString());
        }
        public static void TogglePsychedelicMode()
        {
            if (Drawing.gradientMode == true || Drawing.classicErase == true)
            {
                SetHudMessage("Psychedelic Mode Failed:");
                if (Drawing.gradientMode == true) SetHudMessage("  Gradient mode is active.");
                if (Drawing.classicErase == true) SetHudMessage("  Classic erase is active.");
            }
            else
            {
                psychedelicMode = !psychedelicMode;
                SetHudMessage("Psychedelic Mode: " + psychedelicMode.ToString());
            }
        }

        public static void SetTailLength(int length)
        {
            Drawing.trailLength = length * 10; //gives a length from 10 to 90
            SetHudMessage("Length = " + trailLength.ToString());
        }
        //called by f1, f2, f3.  Cycles through the color list for one of the three colors
        public static void IncrementColor(int whichColor)
        {
            int index = colorIndexes[whichColor];
            index++;
            if (index > 15)
                index = 0;
            SetPen(index, whichColor);
            SetHudMessage("Color " + whichColor.ToString() + " set to " + index.ToString());
        }

        /*
         * called from f-4 press.  This gets three random #s from 0-15, and sets it to each color.
         */
        public static void RandomizeColor(bool showHud = true)
        {
            SetPen(rnd.Next(0, 15), 0);
            SetPen(rnd.Next(0, 15), 1);
            SetPen(rnd.Next(0, 15), 2);
            if (showHud == true) SetHudMessage("Colors randomized");
        }
        public static void CyclePenThickness(int index)
        {
            if (Drawing.penThickness[index] < 6)//real-time. 
            {
                Drawing.penThickness[index]++;
            }
            else
            {
                Drawing.penThickness[index] = 1;
            }
            SetHudMessage("Pen " + index.ToString() + " thickness set to " + Drawing.penThickness[index].ToString());
        }
        public static void CycleMaxIncrement()
        {
            if (Drawing.maxIncrement < 64)//trickery to cycle powers of 2 from 4 to 64
            {
                Drawing.maxIncrement *= 2;
            }
            else
            {
                Drawing.maxIncrement = 4;
            }
            SetHudMessage("Max Increment set to " + Drawing.maxIncrement.ToString());
        }
        public static void CycleMinIncrement()
        {
            Drawing.minIncrement ^= 6; //trickery to toggle between 2 and 4 by xoring 6
            SetHudMessage("Min Increment set to " + Drawing.minIncrement.ToString());

        }
        
        //end setters
        public static void CycleDrawMode()
        {
            drawMode = ++drawMode % 3;
            if (drawMode == 0)
                SetHudMessage("Lines");
            else if (drawMode == 1)
                SetHudMessage("Rectangles");
            else if (drawMode == 2)
                SetHudMessage("Ellipses");
        }
        //runs the logic that shows (or doesn't show) the HUD on the screen 
        public static void DrawHUDMessage(ref PaintEventArgs e)
        {
            if (hudActive == false)
                return;
            bool drewSomething = false;
            if (hudMessages.Length > 25)  //remove first item from array (list is too long)
            {
                Array.ConstrainedCopy(hudMessages, 1, hudMessages, 0, hudMessages.Length - 1);
                Array.Resize(ref hudMessages, hudMessages.Length - 1);
            }

            for (int i = hudMessages.GetUpperBound(0); i >= 0; i--)
            {
                if (hudMessages[i].hudCounter != 0)
                {
                    hudMessages[i].hudCounter--;
                    Color color = getGradientColor(Color.Yellow, Color.Black, hudMessages[i].getMaxCount() - hudMessages[i].hudCounter, hudMessages[i].getMaxCount());
                    DrawText(hudMessages[i].hudMessage, ref e, i, color, 1);
                    drewSomething = true;
                }
            }
            if (drewSomething == false)
                hudMessages = new HudMessage[0];
        }

        public static void DrawTrail(ref PaintEventArgs e)
        {
            if (psychedelicMode == true)
                Drawing.RandomizeColor(false);
            Graphics g;
            if (classicErase == true)
            {
                try
                {
                    g = Graphics.FromImage(Program.onlyForm.BackgroundImage); //for drawing on the background image directly
                    width = Program.onlyForm.BackgroundImage.Width;
                    height = Program.onlyForm.BackgroundImage.Height;
                }
                catch (NullReferenceException)
                {
                    return; //prevent a crash when you lose focus
                }
            }
            else
            {
                g = e.Graphics;//for drawing on the buffer
                width = (int)e.Graphics.ClipBounds.Width;
                height = (int)e.Graphics.ClipBounds.Height;
            }
            if (paused == false) //if paused, stop incrementing through the tail, giving the appearance of paused.
            {
                
                Line line = new Line();
                line.start.X = trail[trailStartMarker].start.X;// * read current line from array
                line.start.Y = trail[trailStartMarker].start.Y;
                line.end.X = trail[trailStartMarker].end.X;
                line.end.Y = trail[trailStartMarker].end.Y;
                line.colorIndex = trail[trailStartMarker].colorIndex;
                // * 
                line = SetLineToNewCoords(line, width, height);//moves line.  Also checks for out-of-bounds and reverses direction
                                                                                               // * --check if random color is turned on.  if it is, set new colors 1% chance on each line drawn
                                                                                               // * --if trail length / 3 lines have been drawn, loop through the three available colors.
                IncrementMarkers();// * increment indexes
                trail[trailStartMarker] = line;// * store new workline into array at new index
                trail[trailStartMarker].start.X = line.start.X;
                trail[trailStartMarker].start.Y = line.start.Y;
                trail[trailStartMarker].end.X = line.end.X;
                trail[trailStartMarker].end.Y = line.end.Y;
                trail[trailStartMarker].colorIndex = line.colorIndex;
            }
            // * draw the trail
            //for (int dl = trailLength; dl >= 0; dl--)
            for (int dl = 0; dl <= trailLength; dl++)
            {
                
                int index = trailStartMarker - ( trailLength - dl);
                if (index >= maxTrailLength) { index -= maxTrailLength; }
                if (index < 0) { index += maxTrailLength; }

                /*
                 * you're about to see some UGLY math here.  to mirror, i have to not only scale the existing points
                 * to half-size in one axis, i have to reverse them and then redraw on the other screen half.
                 * For 2x mirroring i have to do this on each axis.
                 */
                Pen localPen;
                if (classicErase && dl == 0)
                {
                    localPen = new Pen(Color.Black, penThickness[trail[index].colorIndex]);
                }
                else {
                    if (gradientMode && !fadingTrail)
                        localPen = new Pen(trail[index].rgbColor, penThickness[trail[index].colorIndex]);
                    else if (gradientMode && fadingTrail)
                        localPen = new Pen(getGradientColor(trail[index].rgbColor, Color.Black, trailLength - dl, trailLength), penThickness[trail[index].colorIndex]);
                    else if (!gradientMode && fadingTrail)
                        localPen = new Pen(getGradientColor(Drawing.lstColors[Drawing.colorIndexes[trail[index].colorIndex]], Color.Black, trailLength - dl, trailLength), penThickness[trail[index].colorIndex]);
                    //localPen = new Pen(trail[index].rgbColor, penThickness[trail[index].colorIndex]);
                    else
                        localPen = new Pen(Drawing.lstColors[Drawing.colorIndexes[trail[index].colorIndex]], penThickness[trail[index].colorIndex]);
                }
                if (showEndCaps == true) //show the little balls on the ends of the lines
                {
                    localPen.StartCap = LineCap.RoundAnchor;
                    localPen.EndCap = LineCap.RoundAnchor;
                }

                if (classicErase == false || dl == trailLength || dl == 0) //basically don't draw the intermediate lines if in classic mode
                {
                    int hDivide = hSplit ? divisor : 1; //tells it to make the width half-size unless overridden by collision divisor
                    int vDivide = vSplit ? divisor : 1; //same as above but vertical
                    int hReverseOffset = reverse ? width : 0; //either will subtract the negative width or zero to reverse lines
                    int vReverseOffset = reverse ? height : 0; //either will subtract the negative width or zero to reverse lines
                    Point start = new Point(trail[index].start.X / hDivide, trail[index].start.Y/ vDivide);
                    Point end = new Point(trail[index].end.X /hDivide, trail[index].end.Y / vDivide);

                    Rectangle r = new Rectangle(); //a copy of the base line for drawing.
                    r.X = Math.Min(trail[index].start.X, trail[index].end.X) / hDivide;
                    r.Width = Math.Abs(trail[index].end.X - trail[index].start.X )/ hDivide;
                    r.Y = Math.Min(trail[index].start.Y, trail[index].end.Y) / vDivide;
                    r.Height = Math.Abs(trail[index].end.Y - trail[index].start.Y) / vDivide;
                    if (drawMode == 1)
                        g.DrawRectangle(localPen, r);
                    else if (drawMode == 0)
                        //upper left line
                        g.DrawLine(localPen, start, end);
                    else if (drawMode == 2)
                        g.DrawEllipse(localPen, r);
                    if (hSplit && vSplit)
                    {
                        //lower right line
                        if (drawMode == 1)
                            //i have to subract the width and height from start so i track this rect by the bottom right corner
                            g.DrawRectangle(localPen, width - r.X - r.Width, height - r.Y - r.Height, r.Width, r.Height);
                        else if (drawMode == 0)
                            g.DrawLine(localPen, width - start.X, height - start.Y, width - end.X, height - end.Y);
                        else if (drawMode == 2)
                            g.DrawEllipse(localPen, width - r.X - r.Width, height - r.Y - r.Height, r.Width, r.Height);
                        if (reverse)
                        {
                            //if double-mirrored, rotate ends for reversal instead of standard mirror
                            hReverseOffset = 0;
                            vReverseOffset = 0;
                            //rotate 90 degrees by swapping x and y.  This requires scaling by aspect ratio.
                            start.X = trail[index].start.Y * width / height /divisor;
                            start.Y = trail[index].start.X * height / width /divisor;
                            end.X = trail[index].end.Y * width / height /divisor;
                            end.Y = trail[index].end.X * height / width /divisor;
                            //now do same code for rectangles
                            r.X = start.X;
                            r.Y = start.Y;
                            r.Width = Math.Abs(end.X - start.X);
                            r.Height = Math.Abs(end.Y - start.Y);
                            if (start.X > end.X) r.X -= r.Width;
                            if (start.Y > end.Y) r.Y -= r.Height;
                        }
                    }
                    if (hSplit == true)
                    {
                        //upper-right line (or right side)
                        if (drawMode == 1)
                            //because rectangles have a width, i have to remove width from xpos to "reverse" the rectangle
                            g.DrawRectangle(localPen, width - r.X - r.Width, Math.Abs(vReverseOffset - r.Y) - (vReverseOffset != 0 ? r.Height: 0), r.Width, r.Height);
                        else if (drawMode == 0)
                            g.DrawLine(localPen, width - start.X, Math.Abs(vReverseOffset - start.Y), width - end.X, Math.Abs(vReverseOffset - end.Y));
                        else if (drawMode == 2)
                            g.DrawEllipse(localPen, width - r.X - r.Width, Math.Abs(vReverseOffset - r.Y) - (vReverseOffset != 0 ? r.Height : 0), r.Width, r.Height);
                    }
                    if (vSplit == true)
                    {
                        //lower-right line (or bottom)
                        if (drawMode == 1)
                        {
                            //because rectangles have a height, i have to remove height from ypos to "flip" the rectangle
                            g.DrawRectangle(localPen, Math.Abs(hReverseOffset - r.X) - (hReverseOffset != 0 ? r.Width : 0), height - r.Y - r.Height, r.Width, r.Height);
                        }
                        else if (drawMode == 0)
                        {
                            g.DrawLine(localPen, Math.Abs(hReverseOffset - start.X), height - start.Y, Math.Abs(hReverseOffset - end.X), height - end.Y);
                        }
                        else
                            g.DrawEllipse(localPen, Math.Abs(hReverseOffset - r.X) - (hReverseOffset != 0 ? r.Width : 0), height - r.Y - r.Height, r.Width, r.Height);
                    }

                }
                localPen.Dispose();
            }
        }

        //this function is for incrementing the counter loops that track the draw and erase lines in the trail array.
        public static void IncrementMarkers()
        {
            if (trailStartMarker < maxTrailLength- 1)
                trailStartMarker++;
            else
                trailStartMarker = 0;
            if (trailEndMarker - trailLength >= 0)//i did this in weird order to prevent the value from EVER going below zero.  The drawing is asynchronous so this matters.
            {
                trailEndMarker = trailStartMarker - trailLength;
            }
            else
            {
                trailEndMarker = maxTrailLength - (trailEndMarker - trailLength);
            }
        }
        //used for the rewind function.
        public static void DecrementMarkers()
        {
            if (trailStartMarker > 0)
                trailStartMarker--;
            else
                trailStartMarker = maxTrailLength - 1;
            if (trailEndMarker - trailLength >= 0)//i did this in weird order to prevent the value from EVER going below zero.  The drawing is asynchronous so this matters.
            {
                trailEndMarker = trailStartMarker - trailLength;
            }
            else
            {
                trailEndMarker = maxTrailLength - (trailEndMarker - trailLength);
            }
        }
        /*
         * This function redraws the last 150 lines, including erasings.  It's purpose is to refresh the view after a clear. 
         * It can be called in any function as it doesn't actually run unless the correct mode is set.
         * todo:  Put the screen erase in here.
         */
        public static void Redraw()  
        {
            if (classicErase == true)
            {
                bool storedPaused = paused;
                paused = true; //so it keeps the same lines when incrementing
                //this doesn't clear the background btw.  It should be blank when this is called.
                Graphics g = g = Graphics.FromImage(Program.onlyForm.BackgroundImage);
                PaintEventArgs e = new PaintEventArgs(g, new Rectangle(0, 0, 4, 4));
                for (int i = 0; i < 150; i++)
                    DecrementMarkers(); //rewind 150 lines
                for (int i = 0; i < 150; i++)
                {
                    IncrementMarkers();
                    DrawTrail(ref e); //redraw them
                }
                paused = storedPaused;
            }

        }

        /// <summary>
        ///  this function has multiple purposes
        /// It takes a passed in line, looks at the magnitude variables(directions)
        /// and uses those values to change the coordinates of the line before returning it.
        /// the line struct also contains a color index so this function increments that if needed.
        /// Finally, after the incrementing is done of all coordinates,
        /// the function verifies no new numbers are out of bounds.If they are, then
        /// they are reset to max or min appropriately and the magnitude is reversed in direction
        /// for that axis, then set to a new magnitude(speed).
        /// </summary>
        /// <param name="line">a struct that will be modified</param>
        /// <param name="maxWidth">current drawing area width</param>
        /// <param name="maxHeight">current drawing area height</param>
        /// <returns></returns>
        public static Line SetLineToNewCoords(Line line, int maxWidth, int maxHeight)
        {
            //this also handles random magnitude changes on wall hits
            line.start.X += Magnitude.start.X;
            if (line.start.X < 0)
            {
                line.start.X = 0; //you can legally draw out of bounds but this prevents errors in display.
                Magnitude.start.X = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.start.X > maxWidth)
            {
                line.start.X = maxWidth;
                Magnitude.start.X = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.start.Y += Magnitude.start.Y;
            if (line.start.Y < 0)
            {
                line.start.Y = 0; //you can legally draw out of bounds but this prevents errors in display.
                Magnitude.start.Y = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.start.Y > maxHeight)
            {
                line.start.Y = maxHeight;
                Magnitude.start.Y = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.end.X += Magnitude.end.X;
            if (line.end.X < 0)
            {
                line.end.X = 0; //you can legally draw out of bounds but this prevents errors in display.
                Magnitude.end.X = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.end.X > maxWidth)
            {
                line.end.X = maxWidth;
                Magnitude.end.X = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            line.end.Y += Magnitude.end.Y;
            if (line.end.Y < 0)
            {
                line.end.Y = 0; //you can legally draw out of bounds but this prevents errors in display.
                Magnitude.end.Y = rnd.Next(minIncrement, maxIncrement);
            }
            else if (line.end.Y > maxHeight)
            {
                line.end.Y = maxHeight;
                Magnitude.end.Y = (rnd.Next(minIncrement, maxIncrement)) * -1;
            }

            // * --note that trail drawing contains the code to adjust for mirrored and split
            //check steps towards next color.  Switch if reached and generate new #
            colorStepsTaken++;
            if (colorStepsTaken >= nextColorCycle)
            {
                colorStepsTaken = 0;
                nextColorCycle = rnd.Next(20, 100);
                line.colorIndex++;
                if (gradientMode) SetPen(rnd.Next(0, 15), (line.colorIndex + 2) % 3);  //because two of the colors are needed for gradient, i randomize only the 3rd
                if (line.colorIndex > 2)
                {
                    line.colorIndex = 0;
                    
                }

            }

            /*
             * The following block of code is quite possibly the most mathematically complex of the entire app.
             * It takes the current color and the future color, then breaks them each down into their component RGB 
             * values.  It then looks at how many lines will be drawn from the current color starting and when the
             * next color appears.  It divides the difference in each color value by the number of steps.
             * Finally, it multiplies that value by the number of steps already taken and adds the start color to it.
             * After combining this color into rgb, you get the new gradient color.
             */
            Color startColor = (Color)lstColors[Drawing.colorIndexes[line.colorIndex]];//starting color of gradient.  Current index.
            int nextIndex = line.colorIndex + 1; //there are only 3 color indexes.  we loop through them.
            if (nextIndex > 2) nextIndex = 0;
            Color endColor = (Color)lstColors[Drawing.colorIndexes[nextIndex]];//ending color of gradient
            line.rgbColor = getGradientColor(startColor, endColor, colorStepsTaken, nextColorCycle);
            return line;
            
        }
        /// <summary>
        /// generic function to get the intermediate color in a fade by splitting it in to component colors,
        /// dividing the start and end differences by the fraction of how far you are into the fade, 
        /// then recombines the result into a new color.
        /// </summary>
        /// <param name="startColor">color you are starting with</param>
        /// <param name="endColor">target color</param>
        /// <param name="stepsTaken">steps taken/total steps = 0-100% faded.  This tells you the current color at the current step you are on.</param>
        /// <param name="totalSteps">how many steps the gradient will use to complete.  Defaults to 50 if not passed.</param>
        /// <returns></returns>
        private static Color getGradientColor(Color startColor, Color endColor, int stepsTaken, int totalSteps = 50)
        {
            int CurrentR = (int)(((double)(endColor.R - startColor.R) / (double)totalSteps) * stepsTaken) + startColor.R;
            int CurrentG = (int)(((double)(endColor.G - startColor.G) / (double)totalSteps) * stepsTaken) + startColor.G;
            int CurrentB = (int)(((double)(endColor.B - startColor.B) / (double)totalSteps) * stepsTaken) + startColor.B;
            return Color.FromArgb(CurrentR, CurrentG, CurrentB); 
        }
        public static void SaveScreenshot(Form frm)
        {
            using (var bmp = new Bitmap(frm.ClientRectangle.Width, frm.ClientRectangle.Height))

            using (Graphics g = Graphics.FromImage(bmp))
            {
                Rectangle tempRect = frm.RectangleToScreen(frm.ClientRectangle);
                g.CopyFromScreen(tempRect.X,
                                 tempRect.Y,
                                 0, 0,
                                 bmp.Size,
                                 CopyPixelOperation.SourceCopy);
            
                Process currentProcess = Process.GetCurrentProcess();
                string fullPath = currentProcess.MainModule.FileName;
                string justThePath = Path.GetDirectoryName(fullPath);
                string filename = justThePath + "\\screenshot" + screenshotCounter.ToString() + ".png";
                screenshotCounter++; //next save will have a new index
                bmp.Save(filename);
            }
        }
    }
}
