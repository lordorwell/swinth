﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;


namespace Swinth
{
    class Audio
    {
        static Audio()
        {
            //this needs to run only once all by itself to attach the playstatechange function
            player.PlayStateChange += new WMPLib._WMPOCXEvents_PlayStateChangeEventHandler(_PlayStateChange);
        }

        //3rd party library preferred but you probably already have this one.
        //i needed to use windows media player so i could detect when the sound was
        //done playing.  Otherwise i would have just used soundplayer.
        //I am not sure how to package the audio files with it at this time, 
        //  so they need to be in the same folder as the bin.
        public static WMPLib.WindowsMediaPlayer player = new WMPLib.WindowsMediaPlayer();
        private static int i = 1;
        private static Process currentProcess = Process.GetCurrentProcess();
        private static string fullPath = currentProcess.MainModule.FileName;
        private static string justThePath = Path.GetDirectoryName(fullPath);
        public static void PlaySong()
        {  
            player.URL = justThePath + "\\" +  i.ToString()  + ".mp3";  //load the mp3s by #
            player.controls.play();
        }
        public static void StopSong()
        {
            player.controls.stop();
        }
        public static void NextSong()
        {
            i++; if (i > 3) i = 1; //set this # to the max # of songs and you can loop through them
            PlaySong();
        }
        public static bool readyForNextSong = false;
        //callback that is triggered when playstate changes.  Only purpose is to detect when song ends and set a flag.
        public static void _PlayStateChange(int newState)
        {
            if (newState == (int)WMPLib.WMPPlayState.wmppsStopped)
            {
                readyForNextSong = true;
            }
            else if (readyForNextSong == true)
                readyForNextSong = false;
        }
    }

}
