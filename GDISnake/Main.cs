﻿/*
 * Swinth 2.0
 * Goal is to be a direct port of Swinth from the Commodore 64 
 * Example video:  https://www.youtube.com/watch?v=bhSWyCvy65g
 * Port by John Lord.
 */
using System;
using System.Drawing;
using System.Timers;
using System.Windows.Forms;


namespace Swinth
{
    public partial class Main : Form
    {
        //global variables here

        SolidBrush brush = new SolidBrush(Color.FromArgb(128, 128, 255));
        System.Timers.Timer timerDemo = new System.Timers.Timer();
        int speed = 1;
        int pageNum = 1;
        bool drawingAFrame = false;
        public Main()
        {
            this.DoubleBuffered = true;  //prevents flickering.  This line must be in the constructor so it is set before the first draw.
            InitializeComponent();

            timerDemo.Elapsed += new ElapsedEventHandler(OnTimedEvent);//OnTimedEvent is a function below that is called when timer triggers
            timerDemo.Interval = 1; //milliseconds
            this.Width = 1280; //set screen to 4x the res of a c-64 by default.  
            this.Height = 900; //--it can be resized

        }
        /*
         * this function enables the timer and puts us in demo mode
         */
        private void StartDemo()
        {
            resetBackgroundImage();
            Drawing.Redraw();
            //    Drawing.initVariables(); (deprecated)
            // Set the Interval to 1 millisecond.  Note: Time is set in Milliseconds
            timerDemo.Enabled = true;
            Audio.PlaySong();
        }

        /*
         * this function puts us back on the help screen.
         */
        private void StopDemo()
        {
            timerDemo.Enabled = false;
            resetBackgroundImage();
            this.Invalidate();
            Audio.StopSong();
        }
        int c = 0;//counter
        /*
         * this function does two things:  
         * It controls the speed of the drawing and checks once a second to see if the song is done
         */

        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {
            if (c % speed == 0)
            {
                
                this.Invalidate();
            }
            c++; if (c > 100)
            {
                c = 0;
                if (Audio.readyForNextSong == true)
                    Audio.NextSong();
            }
        }
        /**
         * Form_Paint is called automatically whenever the window knows it needs to be redrawn.  
         * This allows me to put any code i wish in the paint function. 
         * Variables here are global but only used in form_paint.
         */
        private bool displayMenu = false;
        private void Form_Paint(object sender, PaintEventArgs e)
        {
            if (drawingAFrame == true)
                return;
            drawingAFrame = true;
            if (this.Focused == false)
                return;
            if (timerDemo.Enabled == true)
            {
                Drawing.DrawTrail(ref e);
                Drawing.DrawHUDMessage(ref e);
            }

            if (timerDemo.Enabled == false || displayMenu == true) //displaymenu will overlay the graphics onto the screen
            {
                /*Following block displays the menu by calling a custom drawing function.  
                * GenericMonospace = NOT proportional font, for retro look
                */
                Drawing.SetFont(new Font(FontFamily.GenericMonospace, e.ClipRectangle.Height / 24, FontStyle.Bold | FontStyle.Underline));
                Drawing.DrawText("SWINTH_2.0", ref e, 0);
                Drawing.SetFont(new Font(FontFamily.GenericMonospace, e.ClipRectangle.Height / 24, FontStyle.Regular));
                Drawing.DrawText("A PC PORT OF SWINTH ON C64 BY JOHN LORD", ref e, 2);
                if (timerDemo.Enabled == false)
                {
                    Drawing.DrawText("HIT SPACEBAR TO START", ref e, 4);
                    Drawing.DrawText("PGDN/PGUP CYCLE OPTIONS", ref e, 5);
                }
                if (pageNum == 1)
                {
                    int n = 8;
                    Drawing.DrawText("I      CHANGES MAX INCREMENT 32>16>8>4", ref e, n++, 2);
                    Drawing.DrawText("E      TOGGLES MIN INCREMENT 1<->2", ref e, n++, 2);
                    Drawing.DrawText("H      TOGGLES HORIZONTAL SYMMETRY", ref e, n++, 2);
                    Drawing.DrawText("V      TOGGLES VERTICAL SYMMETRY", ref e, n++, 2);
                    Drawing.DrawText("0-9    CHANGES LENGTH OF THE TAIL", ref e, n++, 2);
                    Drawing.DrawText("O      TOGGLES OVERLAP", ref e, n++, 2);
                    Drawing.DrawText("R      ROTATES OR REVERSES HALF", ref e, n++, 2);
                    Drawing.DrawText("S      CYCLE LINE, RECTANGLE, ELLIPSE", ref e, n++, 2);
                }
                else if (pageNum == 2)
                {
                    int n = 8;
                    Drawing.DrawText("F1     CHANGES COLOR #1", ref e, n++, 2);
                    Drawing.DrawText("F2     CHANGES COLOR #2", ref e, n++, 2);
                    Drawing.DrawText("F3     CHANGES COLOR #3", ref e, n++, 2);
                    Drawing.DrawText("F4     RANDOM COLORS", ref e, n++, 2);
                    Drawing.DrawText("F5     PSYCHEDELIC MODE", ref e, n++, 2);
                    Drawing.DrawText("F6     GRADIENT MODE", ref e, n++, 2);
                    Drawing.DrawText("F7     END CAPS", ref e, n++, 2);
                    Drawing.DrawText("F8     CLASSIC MODE", ref e, n++, 2);
                    Drawing.DrawText("F9     FADING TAIL", ref e, n++, 2);
                    Drawing.DrawText("F12    SCREENSHOT SAVE", ref e, n++, 2);
                }
                else
                {
                    int n = 8;
                    Drawing.DrawText("P      PAUSES VIEW", ref e, n++, 2);
                    Drawing.DrawText("LEFT   REWINDS (WHILE PAUSED)", ref e, n++, 2);
                    Drawing.DrawText("RIGHT  ADVANCES (WHILE PAUSED)", ref e, n++, 2);
                    Drawing.DrawText("Z      CYCLE COLOR 1 BRUSH THICKNESS", ref e, n++, 2);
                    Drawing.DrawText("X      CYCLE COLOR 2 BRUSH THICKNESS", ref e, n++, 2);
                    Drawing.DrawText("C      CYCLE COLOR 3 BRUSH THICKNESS", ref e, n++, 2);
                    Drawing.DrawText("M      OVERLAY MENU", ref e, n++, 2);
                    Drawing.DrawText("UP     SPEED UP", ref e, n++, 2);
                    Drawing.DrawText("DOWN   SLOW DOWN", ref e, n++, 2);
                }
                Drawing.DrawText("ESC     TERMINATES PROGRAM", ref e, 20, 2);
                Drawing.DrawText("SPACE   PLAYS THE NEXT SONG", ref e, 21, 2);
                Drawing.DrawText("MUSIC FROM SYNTH SAMPLE ON C=64", ref e, 23);
            }
            drawingAFrame = false;
        }
        //simply here for real-time drawing while resizing.
        private void resetBackgroundImage()
        {
            //created a fake background image to use for drawing onto. 
            //i have to manage it manually but that is required for the 
            //erasing functionality i wish to have.
            Bitmap imgBackground = new Bitmap(this.ClientRectangle.Width, this.ClientRectangle.Height);
            this.BackgroundImage = imgBackground;
        }
        private void onResize(object sender, EventArgs e)
        {

            resetBackgroundImage();
            this.Height = (int)(this.Width * .7);
            Invalidate();
        }
        /**
         * this is the function that is called on every keypress.
         * It simply flips some variables around for the most part,
         * except for the escape key.  There are two code blocks,
         * and which one runs depends on if demo is active.
         * I went with case statements for readability
         */
        private void Form1_KeyDown(object sender, KeyEventArgs e)
        {
            if (timerDemo.Enabled == false)//menu
            {
                switch (e.KeyCode)
                {
                    case Keys.Escape:
                        this.Close();
                        break;
                    case Keys.Space:
                        StartDemo();
                        break;
                    case Keys.PageDown:
                        pageNum++;
                        if (pageNum == 4) pageNum = 1;
                        Invalidate();
                        break;
                    case Keys.PageUp:
                        pageNum--;
                        if (pageNum == 0) pageNum = 3;
                        Invalidate();
                        break;
                }
            }
            else//demo running
            {
                switch (e.KeyCode)
                {
                    case Keys.Escape://go back to menu
                        StopDemo();
                        break;
                    case Keys.F1: //increment Color1
                        Drawing.IncrementColor(0);
                        break;
                    case Keys.F2: //increment Color2
                        Drawing.IncrementColor(1);
                        break;
                    case Keys.F3: //increment Color3
                        Drawing.IncrementColor(2);
                        break;
                    case Keys.F4: //randomize all three colors
                        Drawing.RandomizeColor(true);
                        break;
                    case Keys.F5: //toggle psychedelic mode
                        Drawing.TogglePsychedelicMode();
                        break;
                    case Keys.F6: //toggle gradient mode
                        Drawing.ToggleGradientMode();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.F7: //toggle end-caps on lines
                        Drawing.ToggleEndCaps();
                        break;
                    case Keys.F8: //toggle erasing tail 
                        Drawing.ToggleClassicErase();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.F9:
                        Drawing.ToggleFadingTrail();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.F12:
                             //this.FormBorderStyle = FormBorderStyle.None;
                        //Invalidate();
                        Drawing.SaveScreenshot(this);
                           //this.FormBorderStyle = FormBorderStyle.Sizable;
                        break;
                    case Keys.D1: //minimum tail length
                    case Keys.D2:
                    case Keys.D3:
                    case Keys.D4://1-9 = 48-57
                    case Keys.D5:
                    case Keys.D6:
                    case Keys.D7:
                    case Keys.D8:
                    case Keys.D9: //maximum tail length
                    case Keys.D0: //secret d0 = no tail
                        Drawing.SetTailLength((int)e.KeyCode - 48); //turns it into a value from 1 to 9
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.S:
                        Drawing.CycleDrawMode();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.I: //cycle max increment (space between lines)
                        Drawing.CycleMaxIncrement();
                        break;
                    case Keys.E: //toggle min increment (space between lines)
                        Drawing.CycleMinIncrement();
                        break;
                    case Keys.H: //toggle horizontal mirror
                        Drawing.ToggleHSplit();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.V: //toggle vertical mirror
                        Drawing.ToggleVSplit();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.R:
                        resetBackgroundImage();
                        Drawing.ToggleReverse(); //reverses the images on the single-split screens on one side.
                        Drawing.Redraw();
                        break;
                    case Keys.M: //secret "toggle menu" display.  Code to switch screens.
                        if (displayMenu == false)
                        {
                            displayMenu = true;
                            pageNum = 1;
                        }
                        else if (displayMenu == true && pageNum != 3)
                            pageNum++;
                        else
                        {
                            displayMenu = false;
                            pageNum = 1;
                        }
                        break;
                    case Keys.O:
                        Drawing.ToggleCollision();
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.P: //secret pause.  Skips relevant code in Drawing class.
                        Drawing.TogglePaused();
                        break;
                    case Keys.Z: //cycle brush 0 thickness. 
                        Drawing.CyclePenThickness(0);
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.X: //cycle brush 1 thickness. 
                        Drawing.CyclePenThickness(1);
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.C: //cycle brush 2 thickness. 
                        Drawing.CyclePenThickness(2);
                        resetBackgroundImage();
                        Drawing.Redraw();
                        break;
                    case Keys.Space: //go to next song
                        Audio.NextSong();
                        break;
                    case Keys.Up: //speed up.  as little as one timer iteration to draw next line
                        speed--; if (speed < 1) speed = 1;
                        break;
                    case Keys.Down: //slow down.  up to 33 iterations of timer to draw next line
                        speed++; if (speed > 33) speed = 33;
                        break;
                    case Keys.Left: // rewind.  Doesn't currently work in tail erase mode properly
                        resetBackgroundImage();
                        Drawing.DecrementMarkers();
                        Drawing.Redraw();
                        break;
                    case Keys.Right: //advance.  Doesn't currently work in tail erase mode properly
                        Drawing.IncrementMarkers();
                        break;

                }
            }
        }
    }
}
